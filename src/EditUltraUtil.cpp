#include "framework.h"

#define AES_IV_MATERIAL	"Copyright by LiHua"

unsigned char		acAesKey[ AES_BLOCK_SIZE ] ;
unsigned char		acAesIv[ AES_BLOCK_SIZE ] ;

void GenarateConfigKey()
{
#define CONFIG_KEY_MATERIAL		CONFIG_KEY_MATERIAL_EDITULTRA CONFIG_KEY_MATERIAL_FILETREE CONFIG_KEY_MATERIAL_TABPAGES CONFIG_KEY_MATERIAL_LEXER
	unsigned char	acSha224Result[ SHA224_DIGEST_LENGTH + 1 ] ;

	memset( acSha224Result , 0x00 , sizeof(acSha224Result) );
	SHA224( (unsigned char*)CONFIG_KEY_MATERIAL , sizeof(CONFIG_KEY_MATERIAL)-2 , acSha224Result );
	memcpy( acAesKey , acSha224Result+(SHA224_DIGEST_LENGTH-AES_BLOCK_SIZE) , AES_BLOCK_SIZE );

	memset( acSha224Result , 0x00 , sizeof(acSha224Result) );
	SHA224( (unsigned char*)AES_IV_MATERIAL , sizeof(AES_IV_MATERIAL)-2 , acSha224Result );
	memcpy( acAesIv , acSha224Result+(SHA224_DIGEST_LENGTH-AES_BLOCK_SIZE) , AES_BLOCK_SIZE );

	return;
}

int AesEncrypt( unsigned char *dec , unsigned char *enc , int len )
{
	AES_KEY		k ;
	unsigned char	iv[ AES_BLOCK_SIZE ] ;
	int		l ;
	int		nret = 0 ;

	nret = AES_set_encrypt_key( (const unsigned char*)acAesKey , 128 , & k ) ;
	if( nret < 0 )
		return -1;

	if( len % AES_BLOCK_SIZE )
		return -2;

	memcpy( iv , acAesIv , sizeof(iv) );
	DEBUGHEXLOGC( acAesKey , sizeof(acAesKey) , "AesEncrypt - key , [%d]bytes" , AES_BLOCK_SIZE )
	DEBUGHEXLOGC( dec , len , "AesEncrypt - dec , [%d]bytes" , len )
	for( l = 0 ; l < len ; l += AES_BLOCK_SIZE )
	{
		DEBUGHEXLOGC( iv , sizeof(iv) , "AesEncrypt - iv , [%d]bytes" , AES_BLOCK_SIZE )
		AES_cbc_encrypt( dec+l , enc+l , AES_BLOCK_SIZE , & k , iv , AES_ENCRYPT );
	}
	DEBUGHEXLOGC( enc , len , "AesEncrypt - enc , [%d]bytes" , len )

	return 0;
}

int AesDecrypt( unsigned char *enc , unsigned char *dec , int len )
{
	AES_KEY		k ;
	unsigned char	iv[ AES_BLOCK_SIZE ] ;
	int		l ;
	int		nret = 0 ;

	nret = AES_set_decrypt_key( (const unsigned char*)acAesKey , 128 , & k ) ;
	if( nret < 0 )
		return -1;

	if( len % AES_BLOCK_SIZE )
		return -2;

	memcpy( iv , acAesIv , sizeof(iv) );
	DEBUGHEXLOGC( acAesKey , sizeof(acAesKey) , "AesDecrypt - key , [%d]bytes" , AES_BLOCK_SIZE )
	DEBUGHEXLOGC( enc , len , "AesDecrypt - enc , [%d]bytes" , len )
	for( l = 0 ; l < len ; l += AES_BLOCK_SIZE )
	{
		DEBUGHEXLOGC( iv , sizeof(iv) , "AesEncrypt - iv , [%d]bytes" , AES_BLOCK_SIZE )
		AES_cbc_encrypt( enc+l , dec+l , AES_BLOCK_SIZE , & k , iv , AES_DECRYPT );
	}
	DEBUGHEXLOGC( dec , len , "AesDecrypt - dec , [%d]bytes" , len )

	return 0;
}

int Encrypt_3DES_ECB_192bits( unsigned char *key_192bits
	, unsigned char *decrypt , long decrypt_len
	, unsigned char *encrypt , long *encrypt_len )
{
	unsigned char		key[ 8 + 1 ] ;
	DES_key_schedule	ks1 ;
	DES_key_schedule	ks2 ;
	DES_key_schedule	ks3 ;

	unsigned char		*decrypt_ptr = NULL ;
	long			offset ;
	unsigned char		*encrypt_ptr = NULL ;

	unsigned char		in[ 8 + 1 ] ;
	unsigned char		out[ 8 + 1 ] ;

	if( decrypt_len < 0 )
		return -1;
	if( decrypt_len == 0 )
		return 0;

	memcpy( key , key_192bits , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks1 );
	memcpy( key , key_192bits + 8 , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks2 );
	memcpy( key , key_192bits + 8 + 8 , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks3 );

	if( encrypt_len )
	{
		if( (*encrypt_len) < ((decrypt_len-1)/8+1)*8 )
			return -2;

		(*encrypt_len) = 0 ;
	}

	decrypt_ptr = decrypt ;
	encrypt_ptr = encrypt ;
	for( offset = 0 ; offset < decrypt_len ; offset += 8 )
	{
		memset( in , 0x00 , sizeof(in) );
		if( offset + 8 > decrypt_len )
			memcpy( in , decrypt_ptr , decrypt_len - offset );
		else
			memcpy( in , decrypt_ptr , 8 );

		memset( out , 0x00 , sizeof(out) );
		DES_ecb3_encrypt( (const_DES_cblock*)in , (const_DES_cblock*)out , & ks1 , & ks2 , & ks3 , DES_ENCRYPT ) ;
		memcpy( encrypt_ptr , out , 8 );

		if( encrypt_len )
			(*encrypt_len) += 8 ;

		decrypt_ptr += 8 ;
		encrypt_ptr += 8 ;
	}

	return 0;
}

int Decrypt_3DES_ECB_192bits( unsigned char *key_192bits
	, unsigned char *encrypt , long encrypt_len
	, unsigned char *decrypt , long *decrypt_len )
{
	unsigned char		key[ 8 + 1 ] ;
	DES_key_schedule	ks1 ;
	DES_key_schedule	ks2 ;
	DES_key_schedule	ks3 ;

	unsigned char		*encrypt_ptr = NULL ;
	unsigned char		*decrypt_ptr = NULL ;
	long			offset ;

	unsigned char		in[ 8 + 1 ] ;
	unsigned char		out[ 8 + 1 ] ;

	if( encrypt_len < 0 )
		return -1;
	if( encrypt_len == 0 )
		return 0;

	memcpy( key , key_192bits , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks1 );
	memcpy( key , key_192bits + 8 , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks2 );
	memcpy( key , key_192bits + 8 + 8 , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks3 );

	if( decrypt_len )
	{
		if( (*decrypt_len) < ((encrypt_len-1)/8+1)*8 )
			return -2;

		(*decrypt_len) = 0 ;
	}

	encrypt_ptr = encrypt ;
	decrypt_ptr = decrypt ;
	for( offset = 0 ; offset < encrypt_len ; offset += 8 )
	{
		memset( in , 0x00 , sizeof(in) );
		if( offset + 8 > encrypt_len )
			memcpy( in , encrypt_ptr , encrypt_len - offset );
		else
			memcpy( in , encrypt_ptr , 8 );

		memset( out , 0x00 , sizeof(out) );
		DES_ecb3_encrypt( (const_DES_cblock*)in , (const_DES_cblock*)out , & ks1 , & ks2 , & ks3 , DES_DECRYPT ) ;
		memcpy( decrypt_ptr , out , 8 );

		if( decrypt_len )
			(*decrypt_len) += 8 ;

		encrypt_ptr += 8 ;
		decrypt_ptr += 8 ;
	}

	return 0;
}

int Encrypt_3DES_CBC_192bits( unsigned char *key_192bits
	, unsigned char *decrypt , long decrypt_len
	, unsigned char *encrypt , long *encrypt_len
	, unsigned char *init_vector )
{
	unsigned char		key[ 8 + 1 ] ;
	DES_key_schedule	ks1 ;
	DES_key_schedule	ks2 ;
	DES_key_schedule	ks3 ;

	unsigned char		ivec[ 24 + 1 ] ;

	if( decrypt_len < 0 )
		return -1;
	if( decrypt_len == 0 )
		return 0;

	memcpy( key , key_192bits , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks1 );
	memcpy( key , key_192bits + 8 , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks2 );
	memcpy( key , key_192bits + 8 + 8 , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks3 );

	if( encrypt_len )
	{
		if( (*encrypt_len) < ((decrypt_len-1)/8+1)*8 )
			return -2;

		(*encrypt_len) = 0 ;
	}

	if( init_vector == NULL )
	{
		memset( ivec , 0x00 , sizeof(ivec) );
	}
	else
	{
		memcpy( ivec , init_vector , sizeof(ivec)-1 );
	}

	DES_ede3_cbc_encrypt( decrypt , encrypt , decrypt_len , & ks1 , & ks2 , & ks3 , (DES_cblock*)ivec , DES_ENCRYPT );

	if( encrypt_len )
		(*encrypt_len) = decrypt_len ;
	if( init_vector )
		memcpy( init_vector , ivec , sizeof(ivec)-1 );

	return 0;
}

int Decrypt_3DES_CBC_192bits( unsigned char *key_192bits
	, unsigned char *encrypt , long encrypt_len
	, unsigned char *decrypt , long *decrypt_len
	, unsigned char *init_vector )
{
	unsigned char		key[ 8 + 1 ] ;
	DES_key_schedule	ks1 ;
	DES_key_schedule	ks2 ;
	DES_key_schedule	ks3 ;

	unsigned char		ivec[ 24 + 1 ] ;

	if( encrypt_len < 0 )
		return -1;
	if( encrypt_len == 0 )
		return 0;

	memcpy( key , key_192bits , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks1 );
	memcpy( key , key_192bits + 8 , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks2 );
	memcpy( key , key_192bits + 8 + 8 , 8 );
	DES_set_key_unchecked( (const_DES_cblock *)key , & ks3 );

	if( decrypt_len )
	{
		if( (*decrypt_len) < ((encrypt_len-1)/8+1)*8 )
			return -2;

		(*decrypt_len) = 0 ;
	}

	if( init_vector == NULL )
	{
		memset( ivec , 0x00 , sizeof(ivec) );
	}
	else
	{
		memcpy( ivec , init_vector , sizeof(ivec)-1 );
	}

	DES_ede3_cbc_encrypt( encrypt , decrypt , encrypt_len , & ks1 , & ks2 , & ks3 , (DES_cblock*)ivec , DES_DECRYPT );

	if( decrypt_len )
		(*decrypt_len) = encrypt_len ;
	if( init_vector )
		memcpy( init_vector , ivec , sizeof(ivec)-1 );

	return 0;
}

int HexExpand( char *HexBuf , int HexBufLen , char *AscBuf )
{
	int	i,j=0;
	char	c;

	for(i=0;i<HexBufLen;i++){
		c=(HexBuf[i]>>4)&0x0f;
		if(c<=9) AscBuf[j++]=c+'0';
		else AscBuf[j++]=c+'A'-10;

		c=HexBuf[i]&0x0f;
		if(c<=9) AscBuf[j++]=c+'0';
		else AscBuf[j++]=c+'A'-10;
	}
	AscBuf[j]=0;
	return(0);
}

int HexFold( char *AscBuf , int AscBufLen , char *HexBuf )
{
	int		i,j=0,k=1,r=0;
	char		c;
	char		uc;

	if(((int)strlen(AscBuf)<AscBufLen)||(strlen(AscBuf)==0))
		return -1;

	uc=0;
	for(i=0;i<AscBufLen;i++){
		if((AscBuf[i]>='0')&&(AscBuf[i]<='9')) 
			c=(AscBuf[i]-'0')&0x0f;
		else if((AscBuf[i]>='A')&&(AscBuf[i]<='F')) 
			c=(AscBuf[i]-'A'+10)&0x0f;
		else if((AscBuf[i]>='a')&&(AscBuf[i]<='f'))
			c=(AscBuf[i]-'a'+10)&0x0f;
		else {
			r=-1;
			break;
		}
		uc|=(unsigned char)(c<<(k*4));
		k--;
		if(k<0){
			HexBuf[j]=uc;
			uc=0;
			k=1; j++;
		}
	}
	if(k==0) HexBuf[j]=uc;
	return(r);
}

enum FileType GetFileType( char *acPathFileName )
{
	struct _stat	stat_buf ;
	int		nret ;

	nret= _stat( acPathFileName , & stat_buf );
	if( nret == -1 )
		return FILETYPE_ERROR;

	if( stat_buf.st_mode & _S_IFDIR )
		return FILETYPE_DIRECTORY;
	else if( stat_buf.st_mode & _S_IFREG )
		return FILETYPE_REGULAR;
	else
		return FILETYPE_OTHER;
}

char **CommandLineToArgvA( LPWSTR lpCmdLine, int *_argc )
{
	char		**argv = NULL ;
	char		*_argv = NULL ;
	unsigned long	len ;
	unsigned long	argc ;
	char		a ;
	unsigned long	i , j ;

	BOOLEAN  in_QM;
	BOOLEAN  in_TEXT;
	BOOLEAN  in_SPACE;

	BOOL lpUsedDefaultChar = FALSE;
	int retval = WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, NULL, 0, NULL, &lpUsedDefaultChar);
	if (!SUCCEEDED(retval))
	{
		return NULL;
	}

	char *CmdLine = (char*)malloc(retval*sizeof(char)+1);
	if (CmdLine == NULL)
		return NULL;

	lpUsedDefaultChar = FALSE;
	memset( CmdLine , 0x00 , retval*sizeof(char)+1 );
	retval = WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, CmdLine, retval*sizeof(char)+1, NULL, &lpUsedDefaultChar);
	if (!SUCCEEDED(retval))
	{
		return NULL;
	}

	len = (unsigned long)strlen(CmdLine);
	i = ((len+2)/2)*sizeof(PVOID) + sizeof(PVOID);

	argv = (char**)GlobalAlloc(GMEM_FIXED,
		i + (len+2)*sizeof(CHAR));

	_argv = (char*)(((unsigned char *)argv)+i);

	argc = 0;
	argv[argc] = _argv;
	in_QM = FALSE;
	in_TEXT = FALSE;
	in_SPACE = TRUE;
	i = 0;
	j = 0;

	while( a = CmdLine[i] ) {
		if(in_QM) {
			if(a == '\"') {
				in_QM = FALSE;
			} else {
				_argv[j] = a;
				j++;
			}
		} else {
			switch(a) {
			case '\"':
				in_QM = TRUE;
				in_TEXT = TRUE;
				if(in_SPACE) {
					argv[argc] = _argv+j;
					argc++;
				}
				in_SPACE = FALSE;
				break;
			case ' ':
			case '\t':
			case '\n':
			case '\r':
				if(in_TEXT) {
					_argv[j] = '\0';
					j++;
				}
				in_TEXT = FALSE;
				in_SPACE = TRUE;
				break;
			default:
				in_TEXT = TRUE;
				if(in_SPACE) {
					argv[argc] = _argv+j;
					argc++;
				}
				_argv[j] = a;
				j++;
				in_SPACE = FALSE;
				break;
			}
		}
		i++;
	}
	_argv[j] = '\0';
	argv[argc] = NULL;

	(*_argc) = argc;
	return argv;
}

int SetWindowTitle( char *filename )
{
	char	title[100 + MAX_PATH];

	if( filename && filename[0] )
		_snprintf(title, sizeof(title)-1, "%s - %s", g_acAppName, filename );
	else
		_snprintf(title, sizeof(title)-1, "%s", g_acAppName );
	::SetWindowTextA(g_hwndMainWindow, title);

	return 0;
}

BOOL CenterWindow(HWND hWnd, HWND hParent)
{
	RECT rcWnd, rcParent;
	POINT ptNew;
	int nWidth;
	int nHeight;
	int nParentWidth;
	int nParentHeight;

	if (!IsWindow(hWnd))
		return FALSE;

	if (!IsWindow(hParent) || 0 == hParent)
		hParent = GetDesktopWindow();

	GetWindowRect(hWnd, &rcWnd);
	GetWindowRect(hParent, &rcParent);

	nWidth = rcWnd.right - rcWnd.left;
	nHeight = rcWnd.bottom - rcWnd.top;
	nParentWidth = rcParent.right - rcParent.left;
	nParentHeight = rcParent.bottom - rcParent.top;
	ptNew.x = rcParent.left + (nParentWidth - nWidth) / 2;
	ptNew.y = rcParent.top + (nParentHeight - nHeight) / 2;

	return MoveWindow(hWnd, ptNew.x, ptNew.y, nWidth, nHeight, TRUE);
}

char *StrdupEditorSelection( size_t *p_nTextLen , size_t multiple )
{
	size_t	nTextLen ;
	size_t	nMallocLen ;
	char	*pcText = NULL ;

	nTextLen = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELTEXT , 0 , 0 ) - 1 ;
	if( nTextLen > 0 )
	{
		if( multiple > 1 )
			nMallocLen = ((int)(nTextLen/multiple))*multiple + ((nTextLen%multiple)?multiple:0) ;
		else
			nMallocLen = nTextLen ;
		pcText = (char*)malloc( nMallocLen+1) ;
		if( pcText == NULL )
		{
			if( p_nTextLen )
				(*p_nTextLen) = nTextLen ;
			return NULL;
		}
		memset( pcText , 0x00 , nMallocLen+1 );
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELTEXT , nTextLen , (sptr_t)pcText ) ;

		if( p_nTextLen )
			(*p_nTextLen) = nMallocLen ;
		return pcText;
	}
	else
	{
		if( p_nTextLen )
			(*p_nTextLen) = 0 ;
		return NULL;
	}
}

char *StrdupUtf8ToGb( char *utf8_text )
{
	int unicode_len = ::MultiByteToWideChar(CP_UTF8, 0, utf8_text, -1, NULL, 0) ;
	wchar_t *unicode_text = (wchar_t*)malloc( unicode_len * sizeof(wchar_t) ) ;
	if( unicode_text == NULL )
		return NULL;
	memset( unicode_text , 0x00 , unicode_len * sizeof(wchar_t) );
	::MultiByteToWideChar(CP_UTF8, 0, utf8_text, -1, unicode_text, unicode_len);

	int gb_len = ::WideCharToMultiByte(CP_ACP, 0, unicode_text, -1, NULL, 0, NULL, NULL) ;
	char *gb_text = (char*)malloc( gb_len ) ;
	if( gb_text == NULL )
	{
		free( unicode_text );
		return NULL;
	}
	memset( gb_text , 0x00 , gb_len );
	WideCharToMultiByte(CP_ACP,0, unicode_text, -1, gb_text, gb_len, NULL, NULL);
	free( unicode_text );

	return gb_text;
}

char *StrdupGbToUtf8( char *gb_text )
{
	int unicode_len = ::MultiByteToWideChar(CP_ACP, 0, gb_text, -1, NULL, 0) ;
	wchar_t *unicode_text = (wchar_t*)malloc( unicode_len * sizeof(wchar_t) ) ;
	if( unicode_text == NULL )
		return NULL;
	memset( unicode_text , 0x00 , unicode_len * sizeof(wchar_t) );
	::MultiByteToWideChar(CP_ACP, 0, gb_text, -1, unicode_text, unicode_len);

	int utf8_len = ::WideCharToMultiByte(CP_UTF8, 0, unicode_text, -1, NULL, 0, NULL, NULL) ;
	char *utf8_text = (char*)malloc( utf8_len ) ;
	if( utf8_text == NULL )
	{
		free( unicode_text );
		return NULL;
	}
	memset( utf8_text , 0x00 , utf8_len );
	WideCharToMultiByte(CP_UTF8,0, unicode_text, -1, utf8_text, utf8_len, NULL, NULL);
	free( unicode_text );

	return utf8_text;
}

__declspec( thread ) char	tg_acFontName[ 64 ] = "" ;

char *GetFontNameUtf8FromGb( char *fontname_gb )
{
	char	*fontname_utf8 = StrdupGbToUtf8( fontname_gb ) ;

	if( fontname_utf8 == NULL )
		return NULL;

	memset( tg_acFontName , 0x00 , sizeof(tg_acFontName) );
	strncpy( tg_acFontName , fontname_utf8 , sizeof(tg_acFontName)-1 );

	return tg_acFontName;
}

void CopyEditorSelectionToWnd( struct TabPage *pnodeTabPage , HWND hwnd )
{
	char	*text = StrdupEditorSelection( NULL , 0 ) ;
	if( text )
	{
		if( pnodeTabPage->nCodePage == ENCODING_UTF8 )
		{
			char *gb_text = StrdupUtf8ToGb( text ) ;
			if( gb_text == NULL )
			{
				free( text );
				return;
			}
			::SendMessage( hwnd , WM_SETTEXT , 0 , (LPARAM)gb_text );
			free( gb_text );
			free( text );
		}
		else
		{
			::SendMessage( hwnd , WM_SETTEXT , 0 , (LPARAM)text );
			free( text );
		}
	}

	return;
}

void SetMenuItemEnable(HWND hWnd, int nMenuItemId, bool enable)
{
	if (enable)
		::EnableMenuItem(::GetMenu(hWnd), nMenuItemId, MF_ENABLED | MF_BYCOMMAND);
	else
		::EnableMenuItem(::GetMenu(hWnd), nMenuItemId, MF_DISABLED | MF_GRAYED | MF_BYCOMMAND);

	return;
}

void SetMenuItemChecked(HWND hWnd, int nMenuItemId, bool checked)
{
	if (checked)
		::CheckMenuItem(::GetMenu(hWnd), nMenuItemId, MF_CHECKED);
	else
		::CheckMenuItem(::GetMenu(hWnd), nMenuItemId, MF_UNCHECKED);

	return;
}

void InfoBox( const char *format , ... )
{
	char	text[ 1024 ] ;
	va_list	valist ;

	va_start( valist , format );
	memset( text , 0x00 , sizeof(text) );
	vsnprintf( text , sizeof(text)-1 , format , valist );
	va_end( valist );

	::MessageBox(NULL, text, "信息", MB_ICONINFORMATION|MB_OK|MB_TASKMODAL);

	return;
}

void WarnBox( const char *format , ... )
{
	char	text[ 1024 ] ;
	va_list	valist ;

	va_start( valist , format );
	memset( text , 0x00 , sizeof(text) );
	vsnprintf( text , sizeof(text)-1 , format , valist );
	va_end( valist );

	::MessageBox(NULL, text, "信息", MB_ICONWARNING|MB_OK|MB_SYSTEMMODAL|MB_TASKMODAL);

	return;
}

void ErrorBox( const char *format , ... )
{
	char	text[ 1024 ] ;
	va_list	valist ;

	va_start( valist , format );
	memset( text , 0x00 , sizeof(text) );
	vsnprintf( text , sizeof(text)-1 , format , valist );
	va_end( valist );

	::MessageBox(NULL, text, "错误", MB_ICONERROR|MB_OK|MB_SYSTEMMODAL|MB_TASKMODAL);

	return;
}

char* ConvertEncodingEx( const char *encFrom , const char *encTo , const char *p_ori_in , size_t ori_in_len , char *p_ori_out , size_t *p_ori_out_size )
{
	iconv_t		ic ;

	static char	*ori_out = NULL ;
	size_t		ori_out_size ;

	const char	*p_in = NULL ;
	size_t		in_len ;
	char		*p_out = NULL ;
	size_t		out_len = NULL ;

	size_t		sret ;

	/* 打开iconv_t对象 */
	ic = iconv_open( encTo , encFrom ) ;
	if( ic == (iconv_t)-1 )
	{
		return NULL;
	}
	sret = iconv( ic , NULL , NULL , NULL , NULL ) ;

	/* 输入输出指针赋值 */
	p_in = p_ori_in ;
	if( ori_in_len >= 0 )
	{
		in_len = ori_in_len ;
	}
	else
	{
		in_len = strlen(p_ori_in) ;
	}

	if( p_ori_out )
	{
		if( p_ori_out_size )
			ori_out_size = (*p_ori_out_size) ;
		else
			ori_out_size = strlen(p_ori_out) ;
		memset( p_ori_out , 0x00 , ori_out_size );

		p_out = p_ori_out ;
		out_len = ori_out_size ;
	}
	else
	{
		ori_out_size = in_len * 3 + 1 ;
		ori_out = (char*)malloc( ori_out_size ) ;
		if( ori_out == NULL )
			return NULL;
		memset( ori_out , 0x00 , ori_out_size );

		p_out = ori_out ;
		out_len = ori_out_size ;
	}

	/* 编码转换 */
	sret = iconv( ic , (const char **) & p_in , & in_len , (char **) & p_out , & out_len );
	iconv_close( ic ); /* 关闭iconv_t对象 */
	if( sret == -1 || in_len > 0 )
	{
		if( ori_out )
			free( ori_out );
		return NULL;
	}

	/* 返回 */
	if( p_ori_out_size )
		(*p_ori_out_size) = ori_out_size - out_len ;
	if( p_ori_out )
	{
		return p_ori_out;
	}
	else
	{
		return ori_out;
	}
}

void FoldNewLineString( char *str )
{
	char	*p = NULL ;

	while( ( p = strstr( str , "\\r" ) ) )
	{
		*(p) = '\r' ;
		memmove( p+1 , p+2 , strlen(p+2)+1 );
	}

	while( ( p = strstr( str , "\\n" ) ) )
	{
		*(p) = '\n' ;
		memmove( p+1 , p+2 , strlen(p+2)+1 );
	}

	return;
}

void ToUpperString( char *str )
{
	char	*p = NULL ;

	for( p = str ; *(p) ; p++ )
	{
		if( islower(*p) )
			(*p) = toupper(*p) ;
	}

	return;
}

/* 在字符串中整词查询 */
char *strword( char *str , char *word )
{
	char	*copy = NULL ;
	char	*save = NULL ;
	char	*p = NULL ;

	copy = _strdup( str ) ;
	if( copy == NULL )
		return NULL;

	p = strtok_s( copy , " \t" , & save ) ;
	while( p )
	{
		if( STRCMP( p , == , word ) )
		{
			p = p - copy + str ;
			free( copy );
			return p;
		}

		p = strtok_s( NULL , " \t" , & save ) ;
	}

	free( copy );
	return NULL;
}

#include "shobjidl_core.h"

int GetApplicationByFileExt( char *acFileExtName , char *acApplicationName )
{
	IApplicationAssociationRegistration		*pAAR = NULL ;
	/*
	IApplicationAssociationRegistrationInternal	*Paari = NULL;
	*/
	HRESULT						hr ;

	CoInitialize( NULL );

	hr = CoCreateInstance(
		CLSID_ApplicationAssociationRegistration ,
		NULL ,
		CLSCTX_INPROC ,
		__uuidof (IApplicationAssociationRegistration) ,
		(void**) & pAAR ) ;
	if( ! SUCCEEDED(hr) )
		return -1;

	WCHAR	appname[ 256 ] = L"" ;
	LPWSTR	p = NULL ;
	hr = pAAR->QueryCurrentDefault( L".txt" , AT_FILEEXTENSION , AL_EFFECTIVE , & p );
	if( ! SUCCEEDED(hr) )
	{
		pAAR->Release();
		CoUninitialize();
		return -1;
	}

	pAAR->Release();
	CoUninitialize();

	return 0;
}

int SetApplicationByFileExt( char *acFileExtName , char *acApplicationName )
{
	IApplicationAssociationRegistration		*pAAR = NULL ;
	/*
	IApplicationAssociationRegistrationInternal	*Paari = NULL;
	*/
	HRESULT						hr ;

	CoInitialize( NULL );

	hr = CoCreateInstance(
		CLSID_ApplicationAssociationRegistration ,
		NULL ,
		CLSCTX_INPROC ,
		__uuidof (IApplicationAssociationRegistration) ,
		(void**) & pAAR ) ;
	if( ! SUCCEEDED(hr) )
		return -1;

	/*
	hr = pAAR->QueryInterface(
		__uuidof(IApplicationAssociationRegistrationInternal) ,
		(void**) & pAARI ) ;
	if( ! SUCCEEDED(hr) )
		return -2;
	*/
	
	hr = pAAR->SetAppAsDefault(
		L"EUX" ,
		L".txt" ,
		AT_FILEEXTENSION ) ;	
	if( ! SUCCEEDED(hr) )
	{
		pAAR->Release();
		CoUninitialize();

		return -3;
	}

	pAAR->Release();
	CoUninitialize();

	return 0;
}

char *filedup( char *pathfilename , size_t *p_file_len )
{
	char		*file_content = NULL ;
	size_t		file_len ;
	FILE		*fp = NULL ;
	size_t		read_len ;

	int		nret = 0 ;

	fp = fopen( pathfilename , "rb" ) ;
	if( fp == NULL )
	{
		return NULL;
	}

	fseek( fp , 0 , SEEK_END );
	file_len  = ftell( fp ) ;
	fseek( fp , 0 , SEEK_SET );

	file_content = (char*)malloc( file_len+1 ) ;
	if( file_content == NULL )
	{
		fclose( fp );
		return NULL;
	}
	memset( file_content , 0x00 , file_len+1 );

	read_len = fread( file_content , 1 , file_len , fp ) ;
	if( read_len != file_len )
	{
		fclose( fp );
		free( file_content );
		return NULL;
	}

	fclose( fp );

	if( p_file_len )
		(*p_file_len) = file_len ;
	return file_content;
}

#if 0
static unsigned char s_acHexCharSet[] = "0123456789ABCDEF" ;

inline void HexByteFold( unsigned char leftchar , unsigned char rightchar , unsigned char *p_byte )
{
	unsigned char *p1 = (unsigned char *)strchr( (char*)s_acHexCharSet , (int)leftchar ) ;
	unsigned char *p2 = (unsigned char *)strchr( (char*)s_acHexCharSet , (int)rightchar ) ;
	if( p1 == NULL || p2 == NULL )
	{
		(*p_byte) = '\0' ;
		return;
	}
	(*p_byte) = (unsigned char)((p1-s_acHexCharSet)<<4) + (unsigned char)(p2-s_acHexCharSet) ;
	return;
}

inline void HexByteExpand( unsigned char byte , unsigned char doublechar[2+1] )
{
	doublechar[0] = s_acHexCharSet[(byte&0xF0)>>4] ;
	doublechar[1] = s_acHexCharSet[byte&0x0F] ;
	doublechar[2] = '\0' ;
	return;
}
#endif

int QueryNetIpByHostName( char *hostname , char *ip , int ip_bufsize )
{
	struct in_addr	s ;

	struct hostent	*phe = NULL ;
	char		**ip_addr = NULL , *ip_ptr = NULL ;

	int		nret = 0 ;

	memset( ip , 0x00 , ip_bufsize );

	/* 如果是合法IP，原样复制，返回0 */
	memset( & s , 0x00 , sizeof(struct in_addr) );
	nret = inet_pton( AF_INET , hostname , (void*) & s ) ;
	if( nret == 1 )
	{
		strncpy( ip , hostname , ip_bufsize-1 );
		return 0;
	}

	phe = gethostbyname( hostname ) ;
	if( phe )
	{
		switch( phe->h_addrtype )
		{
		case AF_INET :
		case AF_INET6 :
			/* 如果是IP别名，转换之，返回0 */
			ip_addr = phe->h_addr_list ;
#ifdef inet_ntop
			inet_ntop( phe->h_addrtype, (*ip_addr) , ip , ip_bufsize );
#else
			ip_ptr = inet_ntoa( *(struct in_addr*)(*ip_addr) ) ;
			snprintf( ip , ip_bufsize-1 , "%s" , ip_ptr );
#endif
			break;
		default :
			strncpy( ip , hostname , ip_bufsize-1 );
			break;
		}

		return 0;
	}

	/* 如果是无效IP或别名，原样复制，返回-1 */
	strncpy( ip , hostname , ip_bufsize-1 );
	return -1;
}

int GetEditorEffectStartAndEndLine( struct TabPage *pnodeTabPage , int *p_nSelectStartLine , int *p_nSelectEndLine )
{
	if( pnodeTabPage == NULL )
		return -1;

	int nSelectStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETSELECTIONSTART, 0, 0 ) ;
	int nSelectEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETSELECTIONEND, 0, 0 ) ;
	int nSelectStartLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nSelectStartPos, 0 ) ;
	int nSelectEndLine ;

	if( nSelectEndPos - nSelectStartPos > 0 )
	{
		nSelectEndLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nSelectEndPos, 0 ) ;
		if( nSelectEndPos == (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nSelectEndLine, 0 ) )
			nSelectEndLine--;
		if( nSelectEndLine < nSelectStartLine )
			nSelectEndLine = nSelectStartLine ;
	}
	else
	{
		nSelectEndLine = nSelectStartLine ;
	}

	if( p_nSelectStartLine )
		(*p_nSelectStartLine) = nSelectStartLine ;
	if( p_nSelectEndLine )
		(*p_nSelectEndLine) = nSelectEndLine ;

	return 0;
}
