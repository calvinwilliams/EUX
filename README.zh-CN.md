EUX - 中国人开发开源的性能卓越的文本/源码编辑器(内嵌数据库客户端功能和Redis客户端功能)
======================================================================================

<!-- TOC -->

- [1. 引子](#1-%E5%BC%95%E5%AD%90)
- [2. 概述](#2-%E6%A6%82%E8%BF%B0)
	- [2.1. EUX是什么？](#21-eux%E6%98%AF%E4%BB%80%E4%B9%88)
	- [2.2. 功能列表](#22-%E5%8A%9F%E8%83%BD%E5%88%97%E8%A1%A8)
	- [2.3. 特色功能图示](#23-%E7%89%B9%E8%89%B2%E5%8A%9F%E8%83%BD%E5%9B%BE%E7%A4%BA)
- [3. 开发历程](#3-%E5%BC%80%E5%8F%91%E5%8E%86%E7%A8%8B)
- [4. 安装](#4-%E5%AE%89%E8%A3%85)
	- [4.1. 绿色安装](#41-%E7%BB%BF%E8%89%B2%E5%AE%89%E8%A3%85)
	- [4.2. 源码编译安装](#42-%E6%BA%90%E7%A0%81%E7%BC%96%E8%AF%91%E5%AE%89%E8%A3%85)
- [5. 功能导览](#5-%E5%8A%9F%E8%83%BD%E5%AF%BC%E8%A7%88)
	- [5.1. 一级菜单"文件"](#51-%E4%B8%80%E7%BA%A7%E8%8F%9C%E5%8D%95%E6%96%87%E4%BB%B6)
		- [5.1.1. 文件操作](#511-%E6%96%87%E4%BB%B6%E6%93%8D%E4%BD%9C)
		- [5.1.2. 换行符风格](#512-%E6%8D%A2%E8%A1%8C%E7%AC%A6%E9%A3%8E%E6%A0%BC)
		- [5.1.3. 字符编码](#513-%E5%AD%97%E7%AC%A6%E7%BC%96%E7%A0%81)
		- [5.1.4. 远程文件管理器](#514-%E8%BF%9C%E7%A8%8B%E6%96%87%E4%BB%B6%E7%AE%A1%E7%90%86%E5%99%A8)
	- [5.2. 一级菜单"编辑"](#52-%E4%B8%80%E7%BA%A7%E8%8F%9C%E5%8D%95%E7%BC%96%E8%BE%91)
		- [5.2.1. 剪切、复制、粘贴和删除](#521-%E5%89%AA%E5%88%87%E5%A4%8D%E5%88%B6%E7%B2%98%E8%B4%B4%E5%92%8C%E5%88%A0%E9%99%A4)
		- [5.2.2. 合并行](#522-%E5%90%88%E5%B9%B6%E8%A1%8C)
		- [5.2.3. 大小写转换](#523-%E5%A4%A7%E5%B0%8F%E5%86%99%E8%BD%AC%E6%8D%A2)
		- [5.2.4. 启用编辑辅助功能](#524-%E5%90%AF%E7%94%A8%E7%BC%96%E8%BE%91%E8%BE%85%E5%8A%A9%E5%8A%9F%E8%83%BD)
		- [5.2.5. BASE64编解码](#525-base64%E7%BC%96%E8%A7%A3%E7%A0%81)
		- [5.2.6. 散列/消息摘要](#526-%E6%95%A3%E5%88%97%E6%B6%88%E6%81%AF%E6%91%98%E8%A6%81)
		- [5.2.7. DES加解密](#527-des%E5%8A%A0%E8%A7%A3%E5%AF%86)
	- [5.3. 一级菜单"搜索"](#53-%E4%B8%80%E7%BA%A7%E8%8F%9C%E5%8D%95%E6%90%9C%E7%B4%A2)
		- [5.3.1. 查找和替换](#531-%E6%9F%A5%E6%89%BE%E5%92%8C%E6%9B%BF%E6%8D%A2)
		- [5.3.2. 快速选择](#532-%E5%BF%AB%E9%80%9F%E9%80%89%E6%8B%A9)
		- [5.3.3. 高级选择](#533-%E9%AB%98%E7%BA%A7%E9%80%89%E6%8B%A9)
		- [5.3.4. 高级移动光标](#534-%E9%AB%98%E7%BA%A7%E7%A7%BB%E5%8A%A8%E5%85%89%E6%A0%87)
		- [5.3.5. 书签管理](#535-%E4%B9%A6%E7%AD%BE%E7%AE%A1%E7%90%86)
		- [5.3.6. 导航操作](#536-%E5%AF%BC%E8%88%AA%E6%93%8D%E4%BD%9C)
	- [5.4. 一级菜单"视图"](#54-%E4%B8%80%E7%BA%A7%E8%8F%9C%E5%8D%95%E8%A7%86%E5%9B%BE)
		- [5.4.1. 显示/隐藏文件资源树](#541-%E6%98%BE%E7%A4%BA%E9%9A%90%E8%97%8F%E6%96%87%E4%BB%B6%E8%B5%84%E6%BA%90%E6%A0%91)
		- [5.4.2. 选择语言高亮](#542-%E9%80%89%E6%8B%A9%E8%AF%AD%E8%A8%80%E9%AB%98%E4%BA%AE)
		- [5.4.3. 选择主题方案](#543-%E9%80%89%E6%8B%A9%E4%B8%BB%E9%A2%98%E6%96%B9%E6%A1%88)
		- [5.4.4. 修改主题方案](#544-%E4%BF%AE%E6%94%B9%E4%B8%BB%E9%A2%98%E6%96%B9%E6%A1%88)
		- [5.4.5. 复制创建主题](#545-%E5%A4%8D%E5%88%B6%E5%88%9B%E5%BB%BA%E4%B8%BB%E9%A2%98)
		- [5.4.6. 显示/隐藏行号/书签](#546-%E6%98%BE%E7%A4%BA%E9%9A%90%E8%97%8F%E8%A1%8C%E5%8F%B7%E4%B9%A6%E7%AD%BE)
		- [5.4.7. 显示/隐藏白字符](#547-%E6%98%BE%E7%A4%BA%E9%9A%90%E8%97%8F%E7%99%BD%E5%AD%97%E7%AC%A6)
		- [5.4.8. 缩放](#548-%E7%BC%A9%E6%94%BE)
	- [5.5. 一级菜单"编程"](#55-%E4%B8%80%E7%BA%A7%E8%8F%9C%E5%8D%95%E7%BC%96%E7%A8%8B)
		- [5.5.1. 语句块折叠](#551-%E8%AF%AD%E5%8F%A5%E5%9D%97%E6%8A%98%E5%8F%A0)
		- [5.5.2. 源代码符号表](#552-%E6%BA%90%E4%BB%A3%E7%A0%81%E7%AC%A6%E5%8F%B7%E8%A1%A8)
		- [5.5.3. 自动完成和语法提示](#553-%E8%87%AA%E5%8A%A8%E5%AE%8C%E6%88%90%E5%92%8C%E8%AF%AD%E6%B3%95%E6%8F%90%E7%A4%BA)
		- [5.5.4. 数据库SQL文件](#554-%E6%95%B0%E6%8D%AE%E5%BA%93sql%E6%96%87%E4%BB%B6)
		- [5.5.5. REDIS文件](#555-redis%E6%96%87%E4%BB%B6)
	- [5.6. 一级菜单"环境"](#56-%E4%B8%80%E7%BA%A7%E8%8F%9C%E5%8D%95%E7%8E%AF%E5%A2%83)
		- [5.6.1. WINDOWS资源管理器右键菜单](#561-windows%E8%B5%84%E6%BA%90%E7%AE%A1%E7%90%86%E5%99%A8%E5%8F%B3%E9%94%AE%E8%8F%9C%E5%8D%95)
		- [5.6.2. 处理当前文件和当前选择文本的命令](#562-%E5%A4%84%E7%90%86%E5%BD%93%E5%89%8D%E6%96%87%E4%BB%B6%E5%92%8C%E5%BD%93%E5%89%8D%E9%80%89%E6%8B%A9%E6%96%87%E6%9C%AC%E7%9A%84%E5%91%BD%E4%BB%A4)
	- [5.7. 一级菜单"帮助"](#57-%E4%B8%80%E7%BA%A7%E8%8F%9C%E5%8D%95%E5%B8%AE%E5%8A%A9)
	- [5.8. 目前支持语法高亮的语言列表](#58-%E7%9B%AE%E5%89%8D%E6%94%AF%E6%8C%81%E8%AF%AD%E6%B3%95%E9%AB%98%E4%BA%AE%E7%9A%84%E8%AF%AD%E8%A8%80%E5%88%97%E8%A1%A8)
	- [5.9. 启动速度评测](#59-%E5%90%AF%E5%8A%A8%E9%80%9F%E5%BA%A6%E8%AF%84%E6%B5%8B)
- [6. 最后](#6-%E6%9C%80%E5%90%8E)

<!-- /TOC -->

# 1. 引子

你正在寻找一款类似`UltraEdit`强大但免费的文本/源码文件编辑器吗？

你想要一个秒开的开发环境吗？

你还在挑选免费、好用的数据库/Redis图形客户端吗？

**向您推荐`EUX`=文本/源码文件编辑器+数据库客户端+Redis客户端。**

# 2. 概述

## 2.1. EUX是什么？

`EUX`是中国人开发开源的性能卓越的文本/源码文件(内嵌数据库客户端功能和Redis客户端功能)，她基于开源的富文本编辑控件`Scintilla`，自研大量功能，构建出一套完整的文本/源码文件编辑软件，覆盖日常生活工作和源代码编写基本功能，此外`EUX`还能直接提交文本文件里的SQL发给数据库，直接提交Redis命令给Redis服务端。整个软件只有10MB（不包含数据库客户端库），小巧但文本编辑功能完整，且性能卓越，几乎秒开文件，不像VS或VSCODE总是慢半拍。

`EUX`为追求极致的性能体验和极小的系统资源占用，完全使用`C`用`纯WIN32 API`编写而成（v1.0.0.3大约有近2万行代码），又加入了众多优化算法和数据结构，在低端硬件配置上也能飞起。

`EUX`是开源的，任何人都能在其源码托管站点上审阅、下载和学习全部源代码；`EUX`是免费的，任何人无需付费就能使用到全部软件功能；`EUX`是国产软件，如果你有问题反馈或需求提议，可以通过源码托管站点提issue或写电子邮件直接用中文与作者联系沟通。

![images/EUX_hello.png](images/EUX_hello.png)

## 2.2. 功能列表

包含但不限于

* 多文件选项卡；
* WINDOWS资源管理器右键菜单打开文件、目录中所有文件、展开定位到目录树框，还支持目录/文件的创建、改名、删除等；
* 通过SFTP协议远程打开文件，还支持远程目录/文件的创建、改名、删除等；
* 检测文件变动实时重载；
* 文件打开期间自动设置为只读；
* 文件换行符和字符编码转换；
* 高级的切剪、复制和粘贴；
* 查找和替换、一键列出包含关键字行列表（就像grep XXX a.c一样）、自动打开文件所在目录中其他拥有搜索字符串的未打开的文件（就像grep XXX *.*一样）；
* 一键快速选择单词、行；
* 高级移动；
* 高级的累积选择、多位置同步操作、多文本块选择、列选择；
* 选定文本后自动高亮上下文中相同文本；
* BASE64编解码、散列摘要计算和加解密；
* 书签；
* 导航；
* 白字符显示；
* 编程语言语法高亮、自动完成和方法浮动提示，语句块折叠展开；
* 多字体颜色主题方案支持；
* 连接数据库，获取表、字段列表，单行或批量执行SQL，显示查询结果到表格控件中；输入时自动弹出当前库的表、字段列表、SQL语法提示；目前支持`Oracle`、`MySQL`、`PostgreSQL`、`Sqlite3`；
* 连接Redis，单行或批量执行命令，显示查询结果到树控件中；输入时自动弹出Redis命令语法提示；
* 打开JSON/XML文件后解析结构到右边树控件中，按需手动刷新解析；
* 配置执行对文件、选择文本的命令；

## 2.3. 特色功能图示

**不仅可以在WIDNOWS资源管理器里右键打开文件或打开目录中所有文件，还能定位目录到软件内置目录文件树中，方便后续在软件直接管理目录/文件**
![images/EUX_locateforder_in_filetree.png](images/EUX_locateforder_in_filetree.png)
![images/EUX_locateforder_in_filetree_2.png](images/EUX_locateforder_in_filetree_2.png)

**在软件里的目录/文件中直接打开、管理本地/远程文件**
![images/EUX_filetree_popupmenu.png](images/EUX_filetree_popupmenu.png)
![images/EUX_remotefiletree_popupmenu.png](images/EUX_remotefiletree_popupmenu.png)

**选择文本，按Ctrl+Shift+F3自动列出包含该文本的行列表(就像`grep XXX a.c`一样)，双击列表定位到原文件中**
![images/EUX_search_grep.png](images/EUX_search_grep.png)

**查找文本时选择"文件所在目录中已支持类型的文件"，按"查找下一个"，自动搜索当前文件所在目录中包含搜索字符串的其他未打开文件，打开之(就像`grep XXX *.*`一样)**
![images/EUX_search_grep2.png](images/EUX_search_grep2.png)

**按Ctrl+W自动选择光标所在整个单词；按Ctrl+L自动选择当前行**
![images/EUX_select_word.png](images/EUX_select_word.png)
![images/EUX_select_line.png](images/EUX_select_line.png)

**按Ctrl+鼠标左击可以选择非相邻多段文本，然后Ctrl+C拼接复制**
![images/EUX_multiselect.png](images/EUX_multiselect.png)

**按Ctrl+鼠标左击可以生成多个同时操作位置点，后续可以多点同时键入、删除等操作**
![images/EUX_syncoper.png](images/EUX_syncoper.png)

**无需切换模式，按Alt+鼠标左键拖动即可直接列选择**
![images/EUX_columnselect.png](images/EUX_columnselect.png)

**选定某文本后，上下文中相同文本自动高亮**
![images/EUX_indicator_highlight.png](images/EUX_indicator_highlight.png)

**显示空格、制表符、换行等白字符**
![images/EUX_view_whitespace_visiable.png](images/EUX_view_whitespace_visiable.png)

**键入源代码时自动弹出完成列表和函数/方法语法提示(预配置信息有待继续完善)**
![images/EUX_autocompleted.png](images/EUX_autocompleted.png)
![images/EUX_calltip.png](images/EUX_calltip.png)

**打开.sql文件，根据文件头配置自动连接上数据库，右侧树控件中自动列出所有能访问的表、以及表字段清单，选择SQL按F5直接提交数据库执行，查询结果集回显在最下方表格控件中，还可以按Ctrl+F5自动选择光标所在完整SQL语句并执行，还可以全选批量依次执行文本文件中所有SQL；输入SQL时支持自动弹出SQL关键词、当前库的表名、字段名自动完成框、语法提示**
![images/EUX_database_client.png](images/EUX_database_client.png)

**打开.redis文件，根据文件头配置自动连接上Redis服务端，选择Redis命令按F5直接提交执行，查询结果集回显在右方树控件中，还可以按Ctrl+F5自动选择光标所在完整Redis命令语句并执行，还可以全选批量依次执行文本文件中所有Redis命令；输入Redis时支持自动弹出Redis关键词自动完成框、语法提示**
![images/EUX_redis_client.png](images/EUX_redis_client.png)

**打开JSON、XML文件时，右侧树控件中自动显示解析出来的文档结构，双击右侧文档结构某元素，左侧自动定位到该标记开始处**
![images/EUX_filetype_json_parsetree.png](images/EUX_filetype_json_parsetree.png)
![images/EUX_filetype_xml_parsetree.png](images/EUX_filetype_xml_parsetree.png)

# 3. 开发历程

作为一名IT人，总喜欢写些东西，无论是源代码还是文章，从Linux C开发，到写博客，从分析爬虫HTML，到存放SQL命令，从字符编码检测，到建立个人技术知识库，都离不开一款轻便的文本编辑器伴我左右。我认为一款好的文本/源码编辑器，必须要—————快，如果启动要花2秒以上基本可以弃用了，人脑怎么能等电脑呢。

从大学毕业以来尝试过很多文本编辑器，`UltraEdit`是我使用比较长的一款软件，最新版本的`UltraEdit`带来了很多新功能，但是知名品牌“烈火”却迟迟搞不定Hack，我想，从事软件开发积累多年经验的我难道就不能自己写一个吗，需要什么功能就自己加（真香？），并以开放源代码方式发布，于是拾起大学时代的VC撸起袖子直接开干，先取个名字，就叫`EUX`吧。

一开始以为基于开源的富文本编辑控件`Scintilla`可以让我的开发工作神速推进，但阅读完其网上贫瘠的中文资料和官网上也不算丰富的英文文档后，才发现它真的只能当作VC工具箱里的一个稍微高级点的控件而已，大量功能都得自己撸，像多文件选项卡的控制（WINDOWS TABS控件不提供鼠标调整位置，自己增强呗）、远程文件直接打开和保存（Linux C开发必需）、自动完成列表的内容管理（`Scintilla`只提供了调用者给定列表、弹出和关闭接口，而不负责列表的筛选、管理等，如果不精心设计数据结构和算法，反馈体验会很慢）。

软件开发离不开和数据库等打交道，那么发挥一下想象力（好的软件需要想象力），文本编辑器内嵌客户端SDK连接数据库不就能直接执行SQL了吗，避免了大量复制粘贴到数据库客户端软件里的人工操作，而且这年头数据库客户端软件还不便宜，很多看似免费也只是纯个人开发免费，企业/公司中开发还是要买许可，那么既然客户端SDK是免费的，就差一个用户界面把她包装一下了。

经过两个半月，每天晚上写两个小时（周末也是，白天陪全家出去玩等同于上班），一路斩荆披棘，小步迭代而来，终于从v0.0.0.1到v0.0.43.0，很快跳到v1.1.0.0发布了，还请各位看官多多支持和使用，有问题提issue或发邮件给我，我也将努力保持后续的继续迭代，毕竟现在只实现了基本功能，很多低频功能有待于补上。

# 4. 安装

## 4.1. 绿色安装

**下载、解压绿色包**

目前，`EUX`提供WINDOWS(64位/32位)绿色版，免费下载和使用。

`EUX`版本号格式为：`(主版本号)`.`(兼容版本号)`.`(功能版本号)`.`(补丁版本号)`。

`EUX`安装包文件名格式为：`("EUX")`-`(版本号/最新版"latest")`-`("x86"/"x64")`-`(绿色版"noinstall"/安装版"install")-(完整包"full"/更新包"update-based-z.y.x.x"配置文件兼容版本z.y.x.x)`

从以下网址可以查询最新版本更新信息：

[http://114.215.179.129:8080/EUX/ChangeLog-CN](http://114.215.179.129:8080/EUX/ChangeLog-CN)

从以下网址可以下载到最新版本：

包含可执行文件和初始配置文件的64位绿色包完整版

[http://114.215.179.129:8080/EUX/EUX-x64-latest-noinstall-full.zip](http://114.215.179.129:8080/EUX/EUX-x64-latest-noinstall-full.zip)

只包含可执行文件、不包含配置文件的64位绿色包补丁版

[http://114.215.179.129:8080/EUX/EUX-x64-latest-noinstall-update-based-v1.1.x.x.zip](http://114.215.179.129:8080/EUX/EUX-x64-latest-noinstall-update-based-v1.1.x.x.zip)

包含可执行文件和初始配置文件的32位绿色包完整版

[http://114.215.179.129:8080/EUX/EUX-x86-latest-noinstall-full.zip](http://114.215.179.129:8080/EUX/EUX-x86-latest-noinstall-full.zip)

只包含可执行文件、不包含配置文件的32位绿色包补丁版

[http://114.215.179.129:8080/EUX/EUX-x86-latest-noinstall-update-based-v1.1.x.x.zip](http://114.215.179.129:8080/EUX/EUX-x86-latest-noinstall-update-based-v1.1.x.x.zip)

同时，所有类型、历史版本在这里

[http://114.215.179.129:8080/EUX/](http://114.215.179.129:8080/EUX/)

第一次下载完整版，以后下载更新版（防止修改后的配置文件被覆盖），把她放到软件目录中，我一般会放到专门放绿色软件的目录里

```
D:\Program Files\EUX-x64-latest-noinstall-full.zip
```

把里面所有文件解压出来，解压出来只有10MB

```
D:\Program Files\EUX\
```

**运行主程序**

直接运行里面的`EUX.exe`即可启动。

注意：
* 如需内嵌`Oracle`客户端功能，须自行安装`Oracle`，并将WINDOWS环境变量PATH中加入`oci.dll`的所在路径。
* 如需内嵌`MySQL`客户端功能，须自行安装`MySQL`，并将WINDOWS环境变量PATH中加入`libmysql.dll`的所在路径。
* 如需内嵌`PostgreSQL`客户端功能，须自行安装`PostgreSQL`，并将WINDOWS环境变量PATH中加入`libpq.dll`的所在路径。

**注册WINDOWS右键文件/目录弹出菜单项**

如果需要在WINDOWS资源管理器中右键菜单增加“打开文件”功能，执行菜单`环境`->`文件右键弹出菜单`。

![images/EUX_file_popupmenu.png](images/EUX_file_popupmenu.png)

注册成功后就能在WINDOWS里右键打开文件了。

![images/EUX_explorer_openfile.png](images/EUX_explorer_openfile.png)

如果需要在WINDOWS资源管理器中右键菜单增加“打开目录中所有文件”功能，执行菜单`环境`->`目录右键弹出菜单`。

![images/EUX_directory_popupmenu.png](images/EUX_directory_popupmenu.png)

注册成功后就能在WINDOWS里右键打开目录中所有文件了。

![images/EUX_explorer_opendirectory.png](images/EUX_explorer_opendirectory.png)

WINDOWS10操作系统可能会遇到“没有管理员权限”报错，解决方案是右键“以管理员身份运行”`EUX.exe`，再执行以上菜单即可。

## 4.2. 源码编译安装

如果喜欢折腾，全套源代码在

[https://gitee.com/calvinwilliams/EUX](https://gitee.com/calvinwilliams/EUX)

clone下来，用VS自行编译吧，我用的是`Visual Studio 2019`。

编译前需要安装第三方依赖库：SciLexer、pcre、libcurl、iconv、openssl、MySQL、Oracle、PostgreSQL、hiredis。

# 5. 功能导览

## 5.1. 一级菜单"文件"

### 5.1.1. 文件操作

![images/EUX_menu_file.png](images/EUX_menu_file.png)

一级菜单下面对文件的操作有`新建`、`打开...`、`打开最近`、`保存`、`另存为...`、`全部保存`、`关闭`、`关闭所有文件`、`关闭除当前外所有文件`。

启用二级菜单`文件变动检测`可在每次文件选项卡选择该文件时自动检查文件是否变动，如果变动则重载文件。

启用二级菜单`打开文件后设置为只读`可在文件编辑期间临时对文件设置只读权限，防止别人打开修改。

### 5.1.2. 换行符风格

二级菜单`新建文件换行符`设置新建文件时使用的哪个操作系统风格的换行符。二级菜单`全文转换换行符`可在打开文件后调整文件中的换行符风格。换行符风格目前支持`WINDOWS(CR+LF)`、`MAC风格(CR)`、`UNIX/Linux风格(LF)`。

### 5.1.3. 字符编码

二级菜单`新建文件字符编码`设置新建文件时使用的字符编码。二级菜单`全文转换字符编码`可在打开文件后调整文件中的字符编码。字符编码目前支持`UTF-8`、`GB18030`、`BIG5`。

### 5.1.4. 远程文件管理器

![images/EUX_remote_file_manager.png](images/EUX_remote_file_manager.png)

远程文件管理器用于管理远程服务器地址簿。管理器对话框右边是远程服务器信息，填写完后点击`测试远程文件服务器连接`测试，如果测试通过的话，点击`新建远程文件服务器连接`加到左边列表中，注意`连接名称`中不能包含空格等白字符。`更新远程文件服务器连接`会把右边的信息修改到左边的列表中。左下角的`删除远程文件管理器连接`会删除左边列表中的当前选择项。

每个服务器连接信息对应一个配置文件`conf\rfileser_(连接名称).conf`。

如果`密码`不填的话，会在每次启动后第一次展开`文件资源管理器`中该连接时要求输入密码，如果密码正确成功连接上，后续文件操作无需再输入密码。

## 5.2. 一级菜单"编辑"

![images/EUX_menu_edit.png](images/EUX_menu_edit.png)

### 5.2.1. 剪切、复制、粘贴和删除

二级菜单`高级剪切`的三级菜单项`剪切行`用于剪切当前行到剪贴板，无需事前选择，`剪切粘贴行`把当前行移到下一行的下面，剪贴板里遗留一份当前行内容。

二级菜单`高级复制`的三级菜单项`复制行`用于复制当前行到剪贴板，无需事前选择，`复制粘贴行`把当前行向下复制一份，剪贴板里遗留一份当前行内容。

二级菜单`高级粘贴`的三级菜单项`粘贴行`用于把剪贴板里的内容粘贴作为当前行，`往上粘贴行`则是向上粘贴。

二级菜单`高级删除`的三级菜单项`删除行`用于删除当前行，无需事前选择。

### 5.2.2. 合并行

二级菜单`合并行`把下一行拼接到当前行的末尾，无需事前选择。

### 5.2.3. 大小写转换

二级菜单`大小写转换`用于对选择文本一起转换为大写或小写。

### 5.2.4. 启用编辑辅助功能

二级菜单`启用自动补全关闭符号`当在编辑区输入`(`、`[`、`{`、`'`、`"`时自动补全关闭符号，但光标还是定位在输入字符和关闭字符之间。

二级菜单`启用自动缩进`当在编辑区输入回车时，如果当前行左边有白字符（空格、TAB），下一行也会自动加入这些白字符，光标定位在自动补充的白字符后面。

### 5.2.5. BASE64编解码

`EUX`支持用户直接对编辑区选择的文本做BASE64编码，并自动替换原选择文本，编码和解码是可逆的。

### 5.2.6. 散列/消息摘要

`EUX`支持用户直接对编辑区选择的文本做散列/消息摘要，并自动替换原选择文本，摘要算法是不可逆的。目前支持的摘要算法有`MD5`、`SHA1`、`SHA256`。

### 5.2.7. DES加解密

`EUX`支持用户直接对编辑区选择的文本做3DES加解密，弹出对话框输入密钥（24字符，非十六进制展开），加密后按十六进制展开自动替换原选择文本，解密过程同之。

## 5.3. 一级菜单"搜索"

![images/EUX_menu_search.png](images/EUX_menu_search.png)

### 5.3.1. 查找和替换

二级菜单`查找...`弹出查找对话框

![images/EUX_find_dialog.png](images/EUX_find_dialog.png)

可用`普通文本`或`正则表达式`作为源匹配编辑区的文本，可选项`整词匹配`、`大小写匹配`、`匹配单词开始`决定匹配方式，非模态对话框按钮`查找上一个`、`查找下一个`在不关闭查找对话框时定位编辑区匹配字符串处。

二级菜单`查找下一个`（或快捷键`F3`）、`查找上一个`（或快捷键`Ctrl+F3`）在不打开查找对话框直接匹配定位编辑区查找结果。

二级菜单`替换...`弹出替换对话框

![images/EUX_replace_dialog.png](images/EUX_replace_dialog.png)

非模态对话框按钮`替换上一个`、`替换下一个`、`全部替换`在不关闭替换对话框时替换所有编辑区匹配字符串处。

### 5.3.2. 快速选择

二级菜单`全选`或快捷键`Ctrl+A`选择全文，`选择单词组`或快捷键`Ctrl+W`全选当前位置所在单词，`选择行`或快捷键`Ctrl+L`全选当前位置所在行。

### 5.3.3. 高级选择

二级菜单`向右累积选择单词`、`向右累积选择单词组`、`累积选择到下一个语句块首行`用于累加调整选择文本。

假如一个变量名由三个单词组成，单词首字母大小，`向右累积选择单词`自动选择从当前位置到单词最后一个字母，`向右累积选择单词组`自动选择从当前位置到变量名结束，`累积选择到下一个语句块首行`自动选择从当前位置到空行分割的下一个语句块首行。

`向左累积选择*`亦然。

`EUX`支持定位多个位置或选择多个文本块，联动操作：输入、退格键、删除、复制等，快捷键为Ctrl+MouseLButtonClick或Ctrl+MouseLButtonDown+MouseMove+MouseLButtonUp+...。

比如拼接复制

![images/EUX_multiselect.png](images/EUX_multiselect.png)

比如在HTML的TD中同时设置class

![images/EUX_syncoper.png](images/EUX_syncoper.png)

`EUX`支持列选择，联动操作：输入、退格键、删除、复制等，快捷键为Alt+MouseLButton+MouseMove或Alt+Shift+Left/Right/Up/Down。

![images/EUX_columnselect.png](images/EUX_columnselect.png)

作为一个合格的文本/源码编辑器，也支持选定文本后，自动高亮上下中相同的文本

![images/EUX_indicator_highlight.png](images/EUX_indicator_highlight.png)

### 5.3.4. 高级移动光标

同上，但是是移动光标，而不是累积选择。

这些操作配备快捷键实现与`vim`同等的快速处理。

### 5.3.5. 书签管理

二级菜单`书签`提供了对任意打开文件的任意行做行标记，便于事后快速跳跃。

`切换书签`的快捷键是`F9`，`增加书签`的快捷键是`Alt+F9`，`删除书签`的快捷键是`Ctrl+F9`，`删除所有书签`的快捷键是`Ctrl+Shift+F9`。

`跳到上一个书签（当前文件）`的快捷键是`Ctrl+F2`，`跳到下一个书签（当前文件）`的快捷键是`F2`，`跳到上一个书签（所有打开的文件）`的快捷键是`Ctrl+Shift+F2`，`跳到下一个书签（所有打开的文件）`的快捷键是`Shift+F2`。

### 5.3.6. 导航操作

二级菜单`导航`提供了由于鼠标点击定位而产生的位置链的快速退回功能，比如临时到另外一个文件中去复制一个字符串，然后直接跳回来。

`退回到上一个位置（当前文件）`的快捷键是`Ctrl+Back`，`退回到上一个位置（所有打开的文件）`的快捷键是`Ctrl+Shift+Back`。

## 5.4. 一级菜单"视图"

![images/EUX_menu_view.png](images/EUX_menu_view.png)

### 5.4.1. 显示/隐藏文件资源树

二级菜单`文件资源树`用于显示/隐藏主窗口左边的`文件资源管理器`区域。

### 5.4.2. 选择语言高亮

二级菜单`选择语言高亮`用于文件打开后调整文件类型，切换关键词高亮。不过这是临时性的，永久性的做法是在语法高亮配置文件中添加键值，以指定文件类型对应文件扩展名串。

如SQL文件扩展名为";*.sql;"，希望增加一个文件扩展名.prc，加上或修改

```
file.extnames = ";*.sql;*.prc;"
```

注意格式，前后各一个';'，中间是统配表达式集合，多个统配表达式中间也用';'分隔。

### 5.4.3. 选择主题方案

二级菜单`选择主题方案`用于在预置的主题方案中挑一个。

### 5.4.4. 修改主题方案

二级菜单`修改主题方案`用于切换软件启动时装载的主题方案，`conf\styletheme.conf`是默认主题方案，然后此次是`conf\styletheme_*.conf`，如果要在软件里新增，执行二级菜单`复制创建主题方案`将复制当前主题方案到指定名字的新自定义主题方案，并写主题方案配置文件`conf\styletheme_(新名字).conf`。

二级菜单`修改主题方案`用于调整字体颜色方案。

![images/EUX_setfont_dialog.png](images/EUX_setfont_dialog.png)

`EUX`支持编程语言语法高亮、键入时弹出自动完成框、浮动显示语法提示、快速定位到库函数名等符号定义处。`字体颜色方案`对话框左下角为无高亮的普通文本和当前活动行的字体颜色设置，对话框左边为编程语言（C++、Java、Python等）的字体颜色设置，对话框右边为标记语言（HTML、CSS等）的字体颜色设置。

字体颜色方案配置文件在`conf/styletheme.conf`，欢迎大家制作更好看的颜色方案发我，谢谢。

完成支持语言列表见

- [目前支持语法高亮的语言列表](#48-%E7%9B%AE%E5%89%8D%E6%94%AF%E6%8C%81%E8%AF%AD%E6%B3%95%E9%AB%98%E4%BA%AE%E7%9A%84%E8%AF%AD%E8%A8%80%E5%88%97%E8%A1%A8)

### 5.4.5. 复制创建主题

二级菜单`复制主题方案`用于从当前主题方案复制成新的自定义的主题方案，然后自由修改主题方案。

### 5.4.6. 显示/隐藏行号/书签

二级菜单`显示行号`、`显示书签`用于显示/隐藏编辑区靠左的辅助区域里的行号、书签等标记。

### 5.4.7. 显示/隐藏白字符

二级菜单`显示白字符`用于使用可见标记显示空格和TAB，`显示换行符`用于使用可见标记显示换行符，`显示缩进线`用于使用可见标记显示同一缩进行集的缩进竖线。

### 5.4.8. 缩放

二级菜单`缩放`里面的`放大`和`缩小`用于调大字体显示大小，不影响打印大小，不受字体颜色方案影响。可用`重置缩放`恢复原大小。

## 5.5. 一级菜单"编程"

![images/EUX_menu_source.png](images/EUX_menu_source.png)

### 5.5.1. 语句块折叠

二级菜单`启用语句块折叠`用于开启/禁用语句块折叠功能，折叠标记显示在编辑区靠左的辅助区域中。

二级菜单`语句块合拢`和`语句块展开`用于合拢和展开当前层级的语句块，`语句块折叠切换`用于切换合拢和展开。

二级菜单`全部语句块合拢`和`全部语句块展开`用于一键合拢和展开所有层级的语句块。

### 5.5.2. 源代码符号表

`EUX`预置了众多语言的符号提取正则表达式（位于配置文件`conf/doctype_(语言名).conf`中的`symbol_reqular_exp`配置值），匹配出来的符号显示成编辑区右边的符号列表框中，可鼠标左键双击符号直接跳到符号定义处。用户可根据需要，自行修改配置文件以调整匹配式。

二级菜单`跳到符号定义`或快捷键`F11`可直接选定光标所在位置的单词组，从符号列表中查询出编辑区行号，跳到该行。

### 5.5.3. 自动完成和语法提示

`EUX`预置了众多语言的关键字和库函数名，（位于配置文件`conf/doctype_(语言名).conf`中的`autocomplete.set`和`calltip.add`配置值），当用户键入前N个字符时`EUX`会根据优化过的算法和数据结构快速筛选出匹配前N个字符的所有`autocomplete.set`组合，弹出自动完成框提供用户选择，当用户键入`(`、`,`时`EUX`会根据匹配的`calltip.add`，显示语法提示。个别语言可能会有不同的用户操作差异性，比如HTML的属性名自动完成列表是在用户在标记名后键入空格就自动弹出。用户可根据需要，自行修改配置文件以扩展名字空间。

![images/EUX_autocompleted.png](images/EUX_autocompleted.png)

![images/EUX_calltip.png](images/EUX_calltip.png)

### 5.5.4. 数据库SQL文件

如需使用`EUX`数据库客户端功能，须额外安装数据库客户端库，WINDOWS环境变量PATH指向库文件（如`MySQL`的`libmysql.dll`）所在目录，`EUX`在打开`.sql`时会根据配置头动态装载相关动态库。

`EUX`目前支持的数据库有`Oracle`、`MySQL`、`PostgreSQL`、`Sqlite3`。

`EUX`约定了`.sql`为存放数据库SQL的文本文件，额外的，如果发现文件内容以某一特定注释格式（数据库自动连接配置头）出现，则提取相关信息作为连接数据库的参数，在打开文件完成后自动连接数据库，并获取所有能访问的表名列表以及字段信息列表显示到编辑区右边的符号树框中，用户等待连接完成后可直接选择文件后面的SQL并执行，如果执行的是`SELECT`语句还会把查询结果显示在编辑区下面的表格中。

![images/EUX_database_client.png](images/EUX_database_client.png)

`Oracle`的数据库自动连接配置头长这个样子：

```
-- EUX BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : Oracle
--  DBHOST : (SIG)
--  DBPORT : 0
--  DBUSER : (连接用户名)
--  DBPASS : [连接用户密码]
--  DBNAME : 
-- EUX END DATABASE CONNECTION CONFIG
```

以下为一个`Oracle`配置示例：

```
-- EUX BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : Oracle
--  DBHOST : CALVINDB
--  DBPORT : 0
--  DBUSER : calvin
--  DBPASS : 
--  DBNAME : calvindb
-- EUX END DATABASE CONNECTION CONFIG
```

`MySQL`的数据库自动连接配置头长这个样子：

```
-- EUX BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : MySQL
--  DBHOST : (ip)
--  DBPORT : (port)
--  DBUSER : (连接用户名)
--  DBPASS : [连接用户密码]
--  DBNAME : (库名)
-- EUX END DATABASE CONNECTION CONFIG
```

以下为一个`MySQL`配置示例：

```
-- EUX BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : MySQL
--  DBHOST : 127.0.0.1
--  DBPORT : 3306
--  DBUSER : calvin
--  DBPASS : calvin
--  DBNAME : calvindb
-- EUX END DATABASE CONNECTION CONFIG

```

`Sqlite3`的数据库自动连接配置头长这个样子：

```
-- EUX BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : Sqlite3
--  DBHOST : 
--  DBPORT : 
--  DBUSER : 
--  DBPASS : 
--  DBNAME : (sqlite3路径文件名)
-- EUX END DATABASE CONNECTION CONFIG
```

以下为一个`Sqlite3`配置示例：

```
-- EUX BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : Sqlite3
--  DBHOST : 
--  DBPORT : 
--  DBUSER : 
--  DBPASS : 
--  DBNAME : D:\Program Files\test_EUX.sqlite
-- EUX END DATABASE CONNECTION CONFIG

```

`PostgreSQL`的数据库自动连接配置头长这个样子：

```
-- EUX BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : PostgreSQL
--  DBHOST : (ip)
--  DBPORT : (port)
--  DBUSER : (连接用户名)
--  DBPASS : [连接用户密码]
--  DBNAME : (库名)
-- EUX END DATABASE CONNECTION CONFIG
```

以下为一个`PostgreSQL`配置示例：

```
-- EUX BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : PostgreSQL
--  DBHOST : 127.0.0.1
--  DBPORT : 5432
--  DBUSER : postgres
--  DBPASS : postgres
--  DBNAME : calvindb
-- EUX END DATABASE CONNECTION CONFIG

```

注意：DBHOST支持解析域名。
注意：连接用户密码可不配置，在每次连接数据库时会提示输入，如果连接成功后面会记住密码，直到程序结束。

`数据库自动连接配置头`比较难记，二级菜单`插入数据库自动连接配置头`可直接插入配置模板到当前SQL文件中，所以不用刻意去记她，不过每个配置项的含义和用法还是要记忆的。

二级菜单`执行选定SQL`或快捷键`F5`用于执行选定的SQL语句。

注意：右侧表/字段清单、键入表/字段名自动完成列表不会实时追加，需要右键右侧表/字段树点击"刷新"才会同步。

### 5.5.5. REDIS文件

`REDIS自动连接配置头`以及一键执行同数据库。

![images/EUX_redis_client.png](images/EUX_redis_client.png)

## 5.6. 一级菜单"环境"

![images/EUX_menu_env.png](images/EUX_menu_env.png)

### 5.6.1. WINDOWS资源管理器右键菜单

二级菜单`文件右键弹出菜单`用于向WINDOWS资源管理器右键菜单注册/卸载`用 EUX 打开文件`菜单项。

二级菜单`目录右键弹出菜单`用于向WINDOWS资源管理器右键菜单注册/卸载`用 EUX 打开目录中所有文件`菜单项。

注意：在WINDOWS10操作系统中执行以上两个菜单项需要管理员权限，可重新以管理员身份启动`EUX`主程序。

### 5.6.2. 处理当前文件和当前选择文本的命令

`EUX`允许自定义格式命令用于一键处理当前文件或当前编辑区选择文本。

二级菜单`配置处理文件的命令`用于配置处理当前文件的格式命令，用`%F`占位文件名，`执行处理文件的命令`用于执行该命令。

二级菜单`配置处理选定文本的命令`用于配置处理当前选定文本的格式命令，用`%T`占位选定文本，`执行处理选定文本的命令`用于执行该命令，比如打开浏览器提交选定文本为关键字给搜索引擎。

## 5.7. 一级菜单"帮助"

![images/EUX_menu_help.png](images/EUX_menu_help.png)

执行二级菜单`关于`展示`EUX`软件版权和引用第三方库列表。

![images/EUX_version.png](images/EUX_version.png)

## 5.8. 目前支持语法高亮的语言列表

| 语言 | 语法高亮 | 自动完成列表 | 语法提示 | 符号列表与快速定位 |
| --- | --- | --- | --- | --- |
| TXT | - | - | - | - |
| C/C++ | 有 | 有 | 有 | 有 |
| C# | 有 | 有 | - | 有 |
| Java | 有 | 有 | - | 有 |
| JavaScript | 有 | 有 | - | 有 |
| golang | 有 | 有 | - | 有 |
| SWIFT | 有 | 有 | - | 有 |
| SQL | 有 | 有 | 有 | 有 |
| Redis | 有 | 有 | - | 有 |
| Python | 有 | 有 | 有 | 有 |
| Lua | 有 | 有 | 有 | 有 |
| Perl | 有 | - | - | 有 |
| Sh/Bash | 有 | - | - | 有 |
| Rust | 有 | - | - | 有 |
| Ruby | 有 | - | - | 有 |
| Lisp | 有 | - | - | 有 |
| Asm | 有 | 有 | - | 有 |
| Cobol | 有 | 有 | - | 有 |
| HTML | 有 | 有 | - | - |
| ASP | 有 | 有 | - | - |
| PHP | 有 | 有 | - | - |
| VBA | 有 | 有 | - | - |
| XML | 有 | - | - | - |
| CSS | 有 | 有 | - | - |
| JSON | 有 | - | - | - |
| YAML | 有 | - | - | - |
| Makefile | 有 | - | - | - |
| CMake | 有 | 有 | - | - |
| LOG | 有 | - | - | - |
| Nim | 有 | 有 | - | 有 |

注意：LOG文件扩展名为"*.log"，对"TRACE"、"DEBUG"、"INFO"显示为keyword风格，"WARN"、"ERROR"、"FATAL"显示为unknow_tag风格。

目前已支持29种编程语言和标记语言，每种语言的配置文件为`conf/doctype_(语言名).conf`，一般`keywords.set`配置为语法高亮单词列表，`keywords2.set`配置为库函数等第三方符号列表，`autocomplete.set`配置为键入时弹出自动完成单词列表，`calltip.add`为浮动显示语法提示文本，`symbol_reqular_exp`为正则表达式匹配源代码出符号表放到编辑区右边列表框供鼠标左键双击后直接跳到定义处行。

今后还将继续完善已有语言未实现功能，也将增加更多语言，也欢迎大家把急需的语言告知我优先添加，更欢迎大家帮我添加后PR我，谢谢。

## 5.9. 启动速度评测

CPU : i5-7500 3.4GHz
内存 : 16GB
操作系统 : WINDOWS 10
测试说明 : 操作系统启动后，右键测试文本文件弹出菜单中选择打开约28KB大小的文件。

| - | 首次启动耗时 | 非首次启动耗时 |
| --- | --- | --- |
| EUX | 2秒 | 0.5秒 |
| UltraEdit | 5秒 | 1秒 |
| VSCode | 8秒 | 2秒 |

# 6. 最后

`EUX`，中国人开发开源的小巧快速又功能丰富的文本/源码编辑器(内嵌数据库客户端、Redis客户端)。

![images/EUX_hello.png](images/EUX_hello.png)

欢迎使用`EUX`，如果你使用中碰到了问题请提issue或发邮件告诉我，也欢迎帮忙补充自动完成列表、函数/方法语法提示、制作更好的颜色主题给我，谢谢 ^\_^

源码托管地址 : [开源中国](https://gitee.com/calvinwilliams/EUX)

关于作者：厉华，成长在杭州，求学在杭州，工作在杭州，左手C，右手JAVA，写过小到性能卓越方便快捷的日志库、HTTP解析器、日志采集器等，大到交易平台/中间件等，分布式系统实践者，容器技术专研者，2003年大学毕业后一直从事Linux中后台开发，目前在某城市商业银行负责基础架构。

通过邮箱可以联系我 : [网易](mailto:calvinwilliams@163.com)、[Gmail](mailto:calvinwilliams.c@gmail.com)
