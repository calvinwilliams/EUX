@echo off

set PARENT_INSTALL_PATH=D:\Program Files
set INSTALL_PATH=%PARENT_INSTALL_PATH%\EUX32

if not exist "%PARENT_INSTALL_PATH%" (
	mkdir "%PARENT_INSTALL_PATH%"
)

if not exist "%INSTALL_PATH%" (
	mkdir "%INSTALL_PATH%"
)

if not exist "EUX.exe" (
	echo *** ERROR : EUX.exe not found
	exit 1
) else (
	echo "copy EUX.exe %INSTALL_PATH%\"
	copy "EUX.exe" "%INSTALL_PATH%\"
)

if not exist "%INSTALL_PATH%\conf" (
	mkdir "%INSTALL_PATH%\conf"
)
echo "xcopy conf\*.conf %INSTALL_PATH%\conf\"
xcopy /Y "conf\*.conf" "%INSTALL_PATH%\conf\"

if not exist "%INSTALL_PATH%\test" (
	mkdir "%INSTALL_PATH%\test"
)
echo "xcopy test\*.* %INSTALL_PATH%\test\"
xcopy /Y "test\*.*" "%INSTALL_PATH%\test\"

copy /Y AUTHORS "%INSTALL_PATH%\"
copy /Y ChangeLog-CN "%INSTALL_PATH%\"
copy /Y ChangeLog-CN "%INSTALL_PATH%\..\"
copy /Y LICENSE "%INSTALL_PATH%\"
copy /Y LICENSE "%INSTALL_PATH%\"
copy /Y README.md "%INSTALL_PATH%\"
copy /Y README.zh-CN.md "%INSTALL_PATH%\"

REM copy "D:\Work\scintilla-4.3.2\bin\SciLexer.dll" "%INSTALL_PATH%\"

copy "D:\Work\openssl-1.1.1h-win32\bin\libcrypto-1_1.dll" "%INSTALL_PATH%\"
copy "D:\Work\openssl-1.1.1h-win32\bin\libssl-1_1.dll" "%INSTALL_PATH%\"

copy "D:\Work\pcre-7.0-win32\bin\pcre3.dll" "%INSTALL_PATH%\"

copy "D:\Work\curl-7.40.0-devel-mingw32\bin\libcurl.dll" "%INSTALL_PATH%\"
copy "D:\Work\curl-7.40.0-devel-mingw32\bin\libidn-11.dll" "%INSTALL_PATH%\"
copy "D:\Work\curl-7.40.0-devel-mingw32\bin\libeay32.dll" "%INSTALL_PATH%\"
copy "D:\Work\curl-7.40.0-devel-mingw32\bin\ssleay32.dll" "%INSTALL_PATH%\"
copy "D:\Work\curl-7.40.0-devel-mingw32\bin\zlib1.dll" "%INSTALL_PATH%\"

pause
