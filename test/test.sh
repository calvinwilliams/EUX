getConnection $ip $port #函数调用
function getConnection() #函数内容
{
	#timeout命令
	rt=`timeout 3 telnet $1 $2 2>$1` #超过3秒自动停止执行命令
	echo "$rt" |grep Connected >/dev/null
	if [ $? == "0" ];then #$?代表上个命令执行结果，0代表成功，其他代表失败
		echo "telnet $! $2 is ok"
	else
		echo "telnet $! $2 is failed"
	fi
}
